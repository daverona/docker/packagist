#!/bin/ash
set -e

[ ! -z "$TZ" ] && sed -i "s|^;date.timezone =.*$|date.timezone = $TZ|" /etc/php7/php.ini

if [ "supervisord" == "$(basename $1)" ]; then
  # If /app/app/config/parameters.yml is missing, make one with default values.
  if [ ! -f /app/app/config/parameters.yml ]; then
    echo 
    echo "CANNOT FIND /app/app/config/parameters.yml AND IT WILL BE CREATED USING DEFAULT VALUES."
    echo
    cp /app/app/config/parameters.yml.dist /app/app/config/parameters.yml
  fi

  # Build bootstrap, clear cache, and install assets
  composer run --working-dir=/app --no-dev --no-cache post-install-cmd
  # Warms up an empty production cache
  su nginx -s /bin/sh -c "/app/app/console cache:warmup --env=prod"
  # Clean up dev verion stuff
  rm -rf /app/app/cache/dev /app/app/logs/dev.log
fi

exec "$@"
