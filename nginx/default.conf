upstream backend {
  server unix:/var/run/php-fpm.sock;
}

server {
  listen 80;
  listen [::]:80 ipv6only=on;

  server_name _;
  charset utf_8;

  more_clear_headers Server;
  server_tokens off;

  add_header X-Frame-Options "SAMEORIGIN";
  add_header X-XSS-Protection "1; mode=block";
  add_header X-Content-Type-Options "nosniff";

  gzip on;
  gzip_disable "msie6";
  gzip_comp_level 5;
  gzip_min_length 256;
  gzip_proxied any;
  gzip_vary on;
  gzip_static on;
  gzip_types
    application/javascript 
    application/json
    application/rss+xml 
    application/svg+xml
    application/xml 
    application/x-javascript
    text/css 
    text/javascript
    text/js 
    text/plain 
    text/xml;

  root /app/web;
  index index.html index.htm app.php;

  location / {
    try_files $uri $uri/ /app.php?$query_string;
  }

  location ~* \.php$ {
    fastcgi_split_path_info ^(.+?\.php)(/.*)$;
    fastcgi_pass backend;
    fastcgi_index app.php;
    fastcgi_param PATH_INFO $fastcgi_path_info;
    fastcgi_param PATH_TRANSLATED $document_root$fastcgi_path_info;
    fastcgi_param SCRIPT_FILENAME $realpath_root$fastcgi_script_name;
    fastcgi_param DOCUMENT_ROOT $realpath_root;
    include fastcgi_params;
    try_files $uri =404;
  }

  location = /favicon.ico { access_log off; log_not_found off; }
  location = /robots.txt  { access_log off; log_not_found off; }
  # Deny everything prefixed with dot such as .htaccess and .well-known.
  location ~ /\. { deny all; access_log off; log_not_found off; }
}
