# daverona/packagist

[![pipeline status](https://gitlab.com/daverona/docker/packagist/badges/master/pipeline.svg)](https://gitlab.com/daverona/docker/packagist/-/commits/master)

This is a repository for Docker images of [Packagist](https://github.com/composer/packagist).

* GitLab repository: [https://gitlab.com/daverona/docker/packagist](https://gitlab.com/daverona/docker/packagist)
* Docker registry: [https://hub.docker.com/r/daverona/packagist](https://hub.docker.com/r/daverona/packagist)
* Available releases: [https://gitlab.com/daverona/docker/packagist/-/releases](https://gitlab.com/daverona/docker/packagist/-/releases)

## Quick Start

> Aug 18, 2017

Create a database first.

```sql
CREATE USER 'packagist'@'%';
SET PASSWORD FOR 'packagist'@'%' = PASSWORD('secret');
DROP DATABASE IF EXISTS packagist;
CREATE DATABASE packagist DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
GRANT ALL PRIVILEGES ON packagist.* TO 'packagist'@'%';
DROP DATABASE IF EXISTS packagist_test;
CREATE DATABASE packagist_test DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
GRANT ALL PRIVILEGES ON packagist_test.* TO 'packagist'@'%';
FLUSH PRIVILEGES;
```

```bash
docker container run --rm \
  daverona/packagist \
    cat /app/app/config/parameters.yml.dist \
> parameters.yml
```

Edit `parameters.yml`.
Without MariaDB, Redis, and Solr working, you won't get far.

To get configuration files for Solr:

```bash
docker container run --rm --detach --name packagist daverona/packagist
docker cp packagist:/app/doc .
docker container stop packagist
```

and check out `doc` directory.

```bash
docker container run --rm \
  --detach \
  --volume $PWD/parameters.yml:/app/app/config/parameters.yml:ro \
  --publish 80:80 \
  --name packagist \
  daverona/packagist
```

## References

* composer/packagist: [https://github.com/composer/packagist](https://github.com/composer/packagist)
* dockette/packagist: [https://github.com/dockette/packagist](https://github.com/dockette/packagist)
