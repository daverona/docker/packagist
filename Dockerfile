FROM alpine:3.9

ENV LANG=C.UTF-8

# Install packagist and its dependencies
ARG PACKAGIST_COMMIT_SHA=2d90743bec035e87928f4afa356ba28a1547608f
RUN apk add --no-cache \
    composer \
    curl \
    git \
    logrotate \
    mercurial \
    nginx \
    nginx-mod-http-headers-more \
    php7 \
    php7-common \
    php7-ctype \
    php7-dom \
    php7-fpm \
    php7-iconv \
    php7-json \
    php7-mbstring \
    php7-mcrypt \
    php7-mysqlnd \
    php7-openssl \
    php7-pdo \
    php7-pdo_mysql \
    php7-phar \
    php7-session \
    php7-simplexml \
    php7-tokenizer \
    subversion \
    supervisor \
    tzdata \
  && composer self-update \
  # Install packagist
  && git clone https://github.com/composer/packagist /app \
  && git -C /app reset --hard $PACKAGIST_COMMIT_SHA \
  # Delete files and directories for clean exposure
  && rm -rf /app/web/app_dev.php /app/phpunit.xml.dist \
  && (rm -rf /app/.* 2>/dev/null || true) \
  && composer install --working-dir=/app --no-dev --no-interaction --no-scripts --no-cache --optimize-autoloader \
  && rm -rf /root/.composer

# Configure packagist
COPY packagist/schema-6.3.0.xml /app/doc/
COPY cron/ /etc/periodic/minutely/
COPY logrotate/ /etc/logrotate.d/
COPY php-fpm/ /etc/php7/php-fpm.d/
COPY nginx/ /etc/nginx/conf.d/
COPY supervisor/ /etc/supervisor.d/
RUN true \
  # Configure packagist
  && mkdir -p /app/app/cache /app/app/logs \
  && chown -R nginx:nginx /app/app/cache /app/app/logs \
  && chown -R nginx:nginx /app/web \
  # Configure cron
  && mkdir -p /var/log/cron \
  && sed -i "2 a *\t*\t*\t*\t*\trun-parts /etc/periodic/minutely" /etc/crontabs/root \
  # Configure logrotate
  && chmod a+x /etc/periodic/minutely/* \
  # Configure nginx
  && mkdir -p /var/run/nginx

# Configure miscellanea
COPY docker-entrypoint.sh /
RUN chmod a+x /docker-entrypoint.sh 
EXPOSE 80/tcp
WORKDIR /app

ENTRYPOINT ["/docker-entrypoint.sh"]
CMD ["supervisord", "-c", "/etc/supervisord.conf", "-n"]
